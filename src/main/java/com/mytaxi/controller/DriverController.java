package com.mytaxi.controller;

import com.mytaxi.controller.mapper.DriverMapper;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.datatransferobject.SearchTag;
import com.mytaxi.domainobject.DriverCarMappingDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.BadRequest;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.DriverAlreadyHaveOneCarException;
import com.mytaxi.exception.DriverOfflineException;
import com.mytaxi.exception.EntityNotFoundException;
import com.mytaxi.service.driver.DriverService;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.validation.Valid;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * All operations with a driver will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("v1/drivers")
public class DriverController {

    private final DriverService driverService;

    @Autowired
    public DriverController(final DriverService driverService) {
        this.driverService = driverService;
    }


    @GetMapping("/{driverId}")
    public DriverDTO getDriver(@PathVariable long driverId) throws EntityNotFoundException {
        return DriverMapper.makeDriverDTO(driverService.find(driverId));
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DriverDTO createDriver(@Valid @RequestBody DriverDTO driverDTO) throws ConstraintsViolationException {
        DriverDO driverDO = DriverMapper.makeDriverDO(driverDTO);
        return DriverMapper.makeDriverDTO(driverService.create(driverDO));
    }

    @DeleteMapping("/{driverId}")
    public void deleteDriver(@PathVariable long driverId) throws EntityNotFoundException {
        driverService.delete(driverId);
    }


    @PutMapping("/{driverId}")
    public void updateLocation(
            @PathVariable long driverId, @RequestParam double longitude, @RequestParam double latitude)
            throws EntityNotFoundException {
        driverService.updateLocation(driverId, longitude, latitude);
    }


    @PutMapping("/{driverId}/status/{status}")
    public DriverDO updateStatus(
            @PathVariable long driverId, @Valid @PathVariable OnlineStatus status)
            throws EntityNotFoundException {
        return driverService.updateDriverStatus(driverId, status);
    }


    // No need of this API --> we can use search API on this.
//    @GetMapping
//    public List<DriverDTO> findDrivers(@RequestParam OnlineStatus onlineStatus) {
//        return DriverMapper.makeDriverDTOList(driverService.find(onlineStatus));
//    }

    @ApiOperation(value = "Driver select's a Car")
    @PutMapping("/{driverId}/car/select/{carId}")
    public void selectCar(
            @Valid @PathVariable long driverId, @Valid @PathVariable long carId)
            throws EntityNotFoundException, CarAlreadyInUseException,
            DriverAlreadyHaveOneCarException, DriverOfflineException {
        driverService.selectCar(driverId, carId);
    }

    @ApiOperation(value = "Driver Deselect's a Car")
    @PutMapping("/{driverId}/car/deselect/{carId}")
    public void deselectCar(
            @Valid @PathVariable long driverId, @Valid @PathVariable long carId)
            throws EntityNotFoundException {
        driverService.deselectCar(driverId, carId);
    }

    @ApiOperation(value = "Search Driver")
    @GetMapping("/search")
    public List<DriverDTO> searchDriver(
            @RequestParam String searchKeys)
            throws EntityNotFoundException, BadRequest {
        if (searchKeys.trim().isEmpty()) {
            throw new BadRequest("Invalid Search Keys");
        } else {
            List<SearchTag> searchTags = new ArrayList<>();
            Pattern pattern = Pattern.compile("(\\w+.\\w+)([:<>=])(.+),");
            Matcher matcher = pattern.matcher(searchKeys + ",");
            while (matcher.find()) {
                searchTags.add(new SearchTag(matcher.group(1), matcher.group(2), matcher.group(3)));
            }
            return DriverMapper.makeDriverDTOList(driverService.searchDriverOnKeys(searchTags));
        }
    }
}
