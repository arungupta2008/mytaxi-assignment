package com.mytaxi.controller.mapper;

import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.domainobject.CarDO;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by merom on 18/09/18.
 */
public class ServiceModelMapper {

    private static final ModelMapper modelMapper = new ModelMapper();

    // TODO Add Test cases
    public static List<CarDTO> toDto(final List<CarDO> carDO) {
        Type targetListType = new TypeToken<List<CarDTO>>() {
        }.getType();
        return modelMapper.map(carDO, targetListType);
    }

    public static CarDTO toDto(final CarDO carDO) {
        return modelMapper.map(carDO, CarDTO.class);
    }

    public static CarDO toDO(final CarDTO carDTO) {
        return modelMapper.map(carDTO, CarDO.class);
    }
}
