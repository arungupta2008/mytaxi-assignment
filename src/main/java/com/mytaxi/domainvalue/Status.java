package com.mytaxi.domainvalue;

/**
 * Created by merom on 18/09/18.
 */
public enum Status {
    ACTIVE, INACTIVE
}
