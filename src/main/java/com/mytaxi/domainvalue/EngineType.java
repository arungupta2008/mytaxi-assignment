package com.mytaxi.domainvalue;

/**
 * Created by merom on 18/09/18.
 */
public enum EngineType {
    GAS, ELECTRIC, HYBRID
}
