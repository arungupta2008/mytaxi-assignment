package com.mytaxi.domainvalue;

/**
 * Created by merom on 18/09/18.
 */
public enum CarType {
    SEDAN, COMPACT, HATCHBACK, SUV, CROSSOVER, VAN, CONVERTIBLE, TRUCK
}
