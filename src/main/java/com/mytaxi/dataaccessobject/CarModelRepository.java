package com.mytaxi.dataaccessobject;

import com.mytaxi.domainobject.CarModelDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by merom on 18/09/18.
 */
@Repository
public interface CarModelRepository extends JpaRepository<CarModelDO, Long> {

    CarModelDO findDistinctByName(String name);
}
