package com.mytaxi.dataaccessobject;

import com.mytaxi.domainobject.CarDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by merom on 18/09/18.
 */

@Repository
public interface CarRepository extends CrudRepository<CarDO, Long> {

    @Query("SELECT p FROM CarDO p WHERE p.status = 'ACTIVE'")
    List<CarDO> findAllActiveCars();

    @Query("SELECT p FROM CarDO p WHERE p.status = 'ACTIVE' AND p.id = :carId")
    CarDO findActiveCar(@Param("carId") Long carId);
}
