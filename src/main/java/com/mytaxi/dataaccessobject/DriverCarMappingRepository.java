package com.mytaxi.dataaccessobject;

import com.mytaxi.domainobject.DriverCarMappingDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by merom on 18/09/18.
 */
@Repository
public interface DriverCarMappingRepository extends JpaRepository<DriverCarMappingDO, Long> {

    @Query("SELECT p FROM DriverCarMappingDO p WHERE p.status = 'ACTIVE' AND p.driver.id = :driverId")
    DriverCarMappingDO findMapByDriverId(@Param("driverId") Long driverId);

    @Query("SELECT p FROM DriverCarMappingDO p WHERE p.status = 'ACTIVE' AND p.car.id = :carId")
    DriverCarMappingDO findMapByCarId(@Param("carId") Long carId);

    @Query("SELECT p FROM DriverCarMappingDO p WHERE p.status = 'ACTIVE' AND p.car.id = :carId  AND p.driver.id = :driverId")
    DriverCarMappingDO findMapByDriverAndCarId(@Param("driverId") Long driverId, @Param("carId") Long carId);
}