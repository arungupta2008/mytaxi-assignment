package com.mytaxi.dataaccessobject;

import com.mytaxi.domainobject.CarManufacturerDO;
import com.mytaxi.domainobject.CarModelDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by merom on 18/09/18.
 */
public interface CarManufacturerRepository extends JpaRepository<CarManufacturerDO, Long> {

    CarManufacturerDO findDistinctByName(String name);
}
