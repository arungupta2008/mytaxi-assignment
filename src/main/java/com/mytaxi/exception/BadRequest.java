package com.mytaxi.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Bad Request")
public class BadRequest extends Exception{
    public BadRequest(Throwable cause) {
        super(cause);
    }

    public BadRequest(String message) {
        super(message);
    }
}
