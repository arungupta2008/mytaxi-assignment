package com.mytaxi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by merom on 18/09/18.
 */
// State Conflict
@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Conflict in state of Entity")
public class DriverAlreadyHaveOneCarException extends Exception {
    public DriverAlreadyHaveOneCarException(String message) {
        super(message);
    }
}
