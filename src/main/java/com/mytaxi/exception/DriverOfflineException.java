package com.mytaxi.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Conflict in state of Entity")
public class DriverOfflineException extends Exception {
    public DriverOfflineException(String message) {
        super(message);
    }
}
