package com.mytaxi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by merom on 18/09/18.
 */
@ResponseStatus(value = HttpStatus.CONFLICT, reason = "Conflict in state of Entity")
public class CarAlreadyInUseException extends Exception {
    public CarAlreadyInUseException(String message) {
        super(message);
    }
}
