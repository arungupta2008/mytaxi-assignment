package com.mytaxi.service;

import com.mytaxi.domainobject.CarDO;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;

import java.util.List;

/**
 * Created by merom on 18/09/18.
 */
public interface CarService {
    List<CarDO> findAll();
    CarDO find(Long carId) throws EntityNotFoundException;
    CarDO create(CarDO carDO) throws ConstraintsViolationException;
    CarDO update(Long carId, CarDO carDO) throws EntityNotFoundException, ConstraintsViolationException;
    void delete(Long carId) throws EntityNotFoundException;
}
