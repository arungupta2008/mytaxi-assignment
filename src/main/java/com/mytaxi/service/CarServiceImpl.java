package com.mytaxi.service;

import com.mytaxi.dataaccessobject.CarManufacturerRepository;
import com.mytaxi.dataaccessobject.CarModelRepository;
import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.CarManufacturerDO;
import com.mytaxi.domainobject.CarModelDO;
import com.mytaxi.domainvalue.Status;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

/**
 * Created by merom on 18/09/18.
 */

@Service
public class CarServiceImpl implements CarService {


    private static final Logger LOG = LogManager.getLogger(CarServiceImpl.class);
    private final CarRepository carRepository;
    private final CarModelRepository carModelRepository;
    private final CarManufacturerRepository carManufacturerRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository, CarManufacturerRepository carManufacturerRepository, CarModelRepository carModelRepository) {
        this.carRepository = carRepository;
        this.carModelRepository = carModelRepository;
        this.carManufacturerRepository = carManufacturerRepository;
    }

    @Override
    public List<CarDO> findAll() {
        return carRepository.findAllActiveCars();
    }

    @Override
    public CarDO find(Long carId) throws EntityNotFoundException {
        return findActiveCar(carId);
    }

    @Override
    public CarDO create(CarDO carDO) throws ConstraintsViolationException {
        CarDO car;
        try {
            CarModelDO carModelDO = getCarModelDetail(carDO.getModel());
            carDO.setModel(carModelDO);
            car = carRepository.save(carDO);
        } catch (DataIntegrityViolationException e) {
            LOG.warn("Some constraints are thrown due to car creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return carDO;
    }

    private CarModelDO getCarModelDetail(@NotNull(message = "Model can not be null") CarModelDO model) {
        CarModelDO carModelDO = carModelRepository.findDistinctByName(model.getName());
        if (carModelDO == null) {
            CarManufacturerDO carManufacturerDO = carManufacturerRepository.findDistinctByName(model.getManufacturer().getName());
            if (carManufacturerDO == null) {
                return model;
            } else {
                model.setManufacturer(carManufacturerDO);
                return model;
            }
        } else return carModelDO;
    }

    @Override
    public CarDO update(Long carId, CarDO carDO) throws EntityNotFoundException, ConstraintsViolationException {
        CarDO dbCarDo = findActiveCar(carId);
        if (dbCarDo.getModel().getName().equals(carDO.getModel().getName())) {
            carDO.setModel(dbCarDo.getModel());
        }
        carDO.setId(carId);
        try {
            carDO = carRepository.save(carDO);
        } catch (DataIntegrityViolationException e) {
            LOG.warn("Some constraints are thrown due to car creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return carDO;
    }

    @Override
    public void delete(Long carId) throws EntityNotFoundException {
        Optional<CarDO> carDO = carRepository.findById(carId);
        carDO.map(carDO1 -> {
            carDO1.setStatus(Status.INACTIVE);
            return carRepository.save(carDO1);
        }).orElseThrow(() -> new EntityNotFoundException("Entity not Found"));
    }

    private CarDO findActiveCar(Long carId) throws EntityNotFoundException {
        CarDO carDO = carRepository.findActiveCar(carId);
        if (carDO == null) {
            throw new EntityNotFoundException("Could not find entity with id: " + carId);
        } else return carDO;
    }
}
