package com.mytaxi.service.driver;

import com.mytaxi.datatransferobject.SearchTag;
import com.mytaxi.domainobject.DriverCarMappingDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.exception.BadRequest;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.DriverAlreadyHaveOneCarException;
import com.mytaxi.exception.DriverOfflineException;
import com.mytaxi.exception.EntityNotFoundException;

import javax.validation.Valid;
import java.util.List;

public interface DriverService
{

    DriverDO find(Long driverId) throws EntityNotFoundException;

    DriverDO create(DriverDO driverDO) throws ConstraintsViolationException;

    void delete(Long driverId) throws EntityNotFoundException;

    void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException;

    List<DriverDO> find(OnlineStatus onlineStatus);

    DriverCarMappingDO selectCar(@Valid long driverId, @Valid long carId)
            throws DriverAlreadyHaveOneCarException, CarAlreadyInUseException,
            EntityNotFoundException, DriverOfflineException;

    DriverCarMappingDO deselectCar(@Valid long driverId, long carId) throws EntityNotFoundException;

    DriverDO updateDriverStatus(long driverId, OnlineStatus status) throws EntityNotFoundException;

    List<DriverDO> searchDriverOnKeys(List<SearchTag> searchTags) throws BadRequest;
}
