package com.mytaxi.service.driver;

import com.mytaxi.dataaccessobject.DriverCarMappingRepository;
import com.mytaxi.dataaccessobject.DriverRepository;
import com.mytaxi.datatransferobject.SearchTag;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.DriverCarMappingDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.GeoCoordinate;
import com.mytaxi.domainvalue.OnlineStatus;
import com.mytaxi.domainvalue.Status;
import com.mytaxi.exception.BadRequest;
import com.mytaxi.exception.CarAlreadyInUseException;
import com.mytaxi.exception.ConstraintsViolationException;
import com.mytaxi.exception.DriverAlreadyHaveOneCarException;
import com.mytaxi.exception.DriverOfflineException;
import com.mytaxi.exception.EntityNotFoundException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mytaxi.service.CarService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
@Slf4j
public class DefaultDriverService implements DriverService {

    private final DriverRepository driverRepository;
    private final CarService carService;
    private final DriverCarMappingRepository driverCarMappingRepository;

    @PersistenceContext
    private EntityManager entityManager;


    public DefaultDriverService(final DriverRepository driverRepository, CarService carService,
                                DriverCarMappingRepository driverCarMappingRepository) {
        this.driverRepository = driverRepository;
        this.carService = carService;
        this.driverCarMappingRepository = driverCarMappingRepository;
    }


    /**
     * Selects a driver by id.
     *
     * @param driverId
     * @return found driver
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    public DriverDO find(Long driverId) throws EntityNotFoundException {
        return findDriverChecked(driverId);
    }


    /**
     * Creates a new driver.
     *
     * @param driverDO
     * @return
     * @throws ConstraintsViolationException if a driver already exists with the given username, ... .
     */
    @Override
    public DriverDO create(DriverDO driverDO) throws ConstraintsViolationException {
        DriverDO driver;
        try {
            driver = driverRepository.save(driverDO);
        } catch (DataIntegrityViolationException e) {
            log.warn("ConstraintsViolationException while creating a driver: {}", driverDO, e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return driver;
    }


    /**
     * Deletes an existing driver by id.
     *
     * @param driverId
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    @Transactional
    public void delete(Long driverId) throws EntityNotFoundException {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setDeleted(true);
    }


    /**
     * Update the location for a driver.
     *
     * @param driverId
     * @param longitude
     * @param latitude
     * @throws EntityNotFoundException
     */
    @Override
    @Transactional
    public void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setCoordinate(new GeoCoordinate(latitude, longitude));
    }


    /**
     * Find all drivers by online state.
     *
     * @param onlineStatus
     */
    @Override
    public List<DriverDO> find(OnlineStatus onlineStatus) {
        return driverRepository.findByOnlineStatus(onlineStatus);
    }

    @Override
    public DriverCarMappingDO selectCar(@Valid long driverId, @Valid long carId) throws
            DriverAlreadyHaveOneCarException, CarAlreadyInUseException, EntityNotFoundException, DriverOfflineException {
        DriverDO driverDO = findDriverChecked(driverId);

        if (driverDO.getOnlineStatus() == OnlineStatus.OFFLINE) {
            throw new DriverOfflineException("Driver is Offline" + driverDO.getUsername());
        }
        DriverCarMappingDO driverCarMappingDO = driverCarMappingRepository.findMapByDriverId(driverId);
        if (driverCarMappingDO != null) {
            throw new DriverAlreadyHaveOneCarException("Driver " + driverCarMappingDO.getDriver().getUsername() +
                    " already have one active CAR "
                    + driverCarMappingDO.getCar().getLicensePlate());
        }

        DriverCarMappingDO carAssignmentStatus = driverCarMappingRepository.findMapByCarId(carId);
        if (carAssignmentStatus != null) {
            throw new CarAlreadyInUseException("Car " + carAssignmentStatus.getCar().getLicensePlate() +
                    " already assigned to some other Driver" + carAssignmentStatus.getDriver().getUsername());
        }

        CarDO carDO = carService.find(carId);
        return driverCarMappingRepository.save(new DriverCarMappingDO(driverDO, carDO));
    }

    @Override
    public DriverCarMappingDO deselectCar(@Valid long driverId, long carId) throws EntityNotFoundException {
        DriverCarMappingDO driverCarMappingDO = driverCarMappingRepository.findMapByDriverAndCarId(driverId, carId);
        if (driverCarMappingDO == null) {
            throw new EntityNotFoundException("Driver" + driverId + " Car " + carId + " Map  is invalid");
        } else {
            driverCarMappingDO.setStatus(Status.INACTIVE);
            return driverCarMappingRepository.save(driverCarMappingDO);
        }
    }

    @Override
    public DriverDO updateDriverStatus(long driverId, OnlineStatus status) throws EntityNotFoundException {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setOnlineStatus(status);
        return driverRepository.save(driverDO);
    }


    @Override
    public List<DriverDO> searchDriverOnKeys(List<SearchTag> searchTags) throws BadRequest {

        // TODO Fix intellij issue which is giving compile time error for Stream
        final Map<String, List<String>> maps = new HashMap<>();
        for (SearchTag searchTag : searchTags) {
            maps.put(searchTag.getKey(), Arrays.asList(searchTag.getOperation(), searchTag.getSearchValue()));
        }
        return search(entityManager, maps);
    }

    private List search(EntityManager entityManager, Map<String, List<String>> queryAttribute) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("select driver.id, driver.username, driver.password, driver.coordinate," +
                " driver.online_status, driver.date_coordinate_updated, driver.date_created " +
                ", driver.deleted " +
                " from DRIVER_CAR_MAP as dcm " +
                "right outer join driver on driver.id= dcm.driver_id " +
                "left outer join car on car.id =  dcm.car_id where ");

        for (Map.Entry<String, List<String>> entry : queryAttribute.entrySet()) {
            if (entry.getValue().get(0).charAt(0) == ':') {
                stringBuilder.append(entry.getKey()).append(" like ").append("'%").append(entry.getValue().get(1)).append("%'");
            } else {
                stringBuilder.append(entry.getKey()).append(" ").append(entry.getValue().get(0))
                        .append(" '").append(entry.getValue().get(1)).append("'");
            }
        }
        return entityManager.createNativeQuery(stringBuilder.toString(), DriverDO.class).getResultList();
    }


    private DriverDO findDriverChecked(Long driverId) throws EntityNotFoundException {
        return driverRepository.findById(driverId)
                .orElseThrow(() -> new EntityNotFoundException("Could not find entity with id: " + driverId));
    }

}
