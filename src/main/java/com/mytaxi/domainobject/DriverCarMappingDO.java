package com.mytaxi.domainobject;

import com.mytaxi.domainvalue.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

import static com.mytaxi.domainvalue.Status.ACTIVE;

/**
 * Created by merom on 18/09/18.
 */

@Data
@Entity
@Table(name = "driver_car_map")
@AllArgsConstructor
@NoArgsConstructor
//@Builder
public class DriverCarMappingDO extends BaseDomainObject {

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "driver_id")
    @NotNull(message = "Driver Can not be null")
    private DriverDO driver;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "car_id")
    @NotNull(message = "Car Can not be null")
    private CarDO car;
}
