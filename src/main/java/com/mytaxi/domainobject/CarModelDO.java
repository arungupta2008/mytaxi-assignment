package com.mytaxi.domainobject;

import com.mytaxi.domainvalue.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import java.time.ZonedDateTime;

import static com.mytaxi.domainvalue.Status.ACTIVE;
import static com.mytaxi.domainvalue.Status.INACTIVE;

/**
 * Created by merom on 18/09/18.
 */

@Entity
@Table(name = "car_model")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarModelDO extends BaseDomainObject {

    @Column(nullable = false)
    @NotNull(message = "Name can not be null")
    private String name;

    @ManyToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "manufacturer_id")
    @NotNull(message = "Manufacturer can not be null")
    private CarManufacturerDO manufacturer;
}