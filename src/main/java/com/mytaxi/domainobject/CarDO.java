package com.mytaxi.domainobject;

import com.mytaxi.domainvalue.CarType;
import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.time.ZonedDateTime;

import static com.mytaxi.domainvalue.Status.ACTIVE;
import static com.mytaxi.domainvalue.Status.INACTIVE;

/**
 * Created by merom on 18/09/18.
 */

@Entity
@Table(
        name = "car",
        uniqueConstraints = @UniqueConstraint(name = "uc_serial", columnNames = {"serial"})
)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Cacheable(false)
public class CarDO extends BaseDomainObject {

    @Column(nullable = false)
    @NotNull(message = "Serial can not be null")
    private String serial;

    @Column(nullable = false)
    @NotNull(message = "License plate can not be null")
    private String licensePlate;

    @ManyToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "model_id")
    @NotNull(message = "Model can not be null")
    private CarModelDO model;

    @Column(nullable = false)
    @NotNull(message = "Year can not be null")
    private Integer year;

    @Column(nullable = false)
    @NotNull(message = "Color can not be null")
    private String color;

    @Column(nullable = false)
    @NotNull(message = "Seat Count can not be null")
    private Integer seatCount;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull(message = "Engine Type can not be null")
    private EngineType engineType;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull(message = "Car Type can not be null")
    private CarType carType;

    @Column
    private Double raiting;

    @Column(nullable = false)
    private Boolean available = true;
}
