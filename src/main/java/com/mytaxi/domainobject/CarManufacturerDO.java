package com.mytaxi.domainobject;

import com.mytaxi.domainvalue.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.time.ZonedDateTime;

import static com.mytaxi.domainvalue.Status.ACTIVE;
import static com.mytaxi.domainvalue.Status.INACTIVE;

/**
 * Created by merom on 18/09/18.
 */

@Entity
@Table(
        name = "car_manufacturer",
        uniqueConstraints = @UniqueConstraint(name = "uc_name", columnNames = {"name"})
)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarManufacturerDO extends BaseDomainObject {
    @Column(nullable = false)
    @NotNull(message = "Name can not be null")
    @Size(min = 1, message = "Name can not be empty")
    private String name;
}
