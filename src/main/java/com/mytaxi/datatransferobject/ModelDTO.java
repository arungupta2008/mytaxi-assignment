package com.mytaxi.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Created by merom on 18/09/18.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Getter
@Setter
public class ModelDTO {

    @JsonIgnore
    private Long id;

    @NotNull(message = "Name can not be null")
    private String name;

    @NotNull(message = "Name can not be null")
    private ManufacturerDTO manufacturer;
}
