package com.mytaxi.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mytaxi.domainvalue.CarType;
import com.mytaxi.domainvalue.EngineType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by merom on 18/09/18.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Getter
@Setter
public class CarDTO {

    @JsonIgnore
    private Long id;

    @NotNull(message = "Serial can not be null")
    @Size(min = 1, message = "Serial No can not be Empty")
    private String serial;

    @NotNull(message = "License plate can not be null")
    @Size(min = 1, message = "License Place No can not be Empty")
    private String licensePlate;

    @NotNull(message = "Model can not be null")
    private ModelDTO model;

    @NotNull(message = "Year can not be null")
    private Integer year;

    @NotNull(message = "Color can not be null")
    @Size(min = 1, message = "Color can not be Empty")
    private String color;

    @NotNull(message = "Seat Count can not be null")
    private Integer seatCount;

    @NotNull(message = "Engine Type can not be null")
    private EngineType engineType;

    @NotNull(message = "Car Type can not be null")
    private CarType carType;

    private Double raiting;
}