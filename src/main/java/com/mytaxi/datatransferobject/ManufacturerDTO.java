package com.mytaxi.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by merom on 18/09/18.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@Getter
@Setter
public class ManufacturerDTO {

    @JsonIgnore
    private Long id;

    @NotNull(message = "Name can not be null")
    @Size(min = 1, message = "Manufacturer name can not be empty")
    private String name;
}
