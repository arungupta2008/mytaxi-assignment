/**
 * CREATE Script for init of DB
 */

-- Create 3 OFFLINE drivers

insert into driver (id, date_created, deleted, online_status, password, username) values (1, now(), false, 'ONLINE',
'driver01pw', 'driver01');

insert into driver (id, date_created, deleted, online_status, password, username) values (2, now(), false, 'ONLINE',
'driver02pw', 'driver02');

insert into driver (id, date_created, deleted, online_status, password, username) values (3, now(), false, 'ONLINE',
'driver03pw', 'driver03');


-- Create 3 ONLINE drivers

insert into driver (id, date_created, deleted, online_status, password, username) values (4, now(), false, 'ONLINE',
'driver04pw', 'driver04');

insert into driver (id, date_created, deleted, online_status, password, username) values (5, now(), false, 'ONLINE',
'driver05pw', 'driver05');

insert into driver (id, date_created, deleted, online_status, password, username) values (6, now(), false, 'ONLINE',
'driver06pw', 'driver06');

-- Create 1 OFFLINE driver with coordinate(longitude=9.5&latitude=55.954)

insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username)
values
 (7,
 'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'OFFLINE',
'driver07pw', 'driver07');

-- Create 1 ONLINE driver with coordinate(longitude=9.5&latitude=55.954)

insert into driver (id, coordinate, date_coordinate_updated, date_created, deleted, online_status, password, username)
values
 (8,
 'aced0005737200226f72672e737072696e676672616d65776f726b2e646174612e67656f2e506f696e7431b9e90ef11a4006020002440001784400017978704023000000000000404bfa1cac083127', now(), now(), false, 'ONLINE',
'driver08pw', 'driver08');



insert into CAR_MANUFACTURER ("ID","NAME","STATUS", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT  ) values (1, 'Audi', 'ACTIVE', now(), now(), now());

insert into CAR_MANUFACTURER ("ID","NAME","STATUS", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (2, 'Mercedes-AMG', 'ACTIVE', now(), now(), now());

insert into CAR_MANUFACTURER ("ID","NAME","STATUS",CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (3, 'Mercedes', 'ACTIVE', now(), now(), now());

insert into CAR_MANUFACTURER ("ID","NAME","STATUS", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (4, 'Toyota', 'ACTIVE', now(), now(), now());

insert into CAR_MODEL ("ID","NAME","STATUS","MANUFACTURER_ID", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (1, 'Audi 1', 'ACTIVE', 1, now(), now(), now());

insert into CAR_MODEL ("ID","NAME","STATUS","MANUFACTURER_ID", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (2, 'Audi 2', 'ACTIVE', 1, now(), now(), now());

insert into CAR_MODEL ("ID","NAME","STATUS","MANUFACTURER_ID", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (3, 'Audi 3', 'ACTIVE', 1, now(), now(), now());

insert into CAR_MODEL ("ID","NAME","STATUS","MANUFACTURER_ID", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (4, 'Audi 4', 'ACTIVE', 1, now(), now(), now());

insert into CAR_MODEL ("ID","NAME","STATUS","MANUFACTURER_ID", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (5, 'Audi 5', 'ACTIVE', 1, now(), now(), now());

insert into CAR_MODEL ("ID","NAME","STATUS","MANUFACTURER_ID", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (6, 'Mercedes-AMG V1.0', 'ACTIVE', 2, now(), now(), now());

insert into CAR_MODEL ("ID","NAME","STATUS","MANUFACTURER_ID", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (7, 'Mercedes-AMG V2.0', 'ACTIVE', 2, now(), now(), now());

insert into CAR_MODEL ("ID","NAME","STATUS","MANUFACTURER_ID", CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT) values (8, 'Mercedes-AMG V3.0', 'ACTIVE', 2, now(), now(), now());


insert into car (serial, license_plate, model_id, year, color, seat_count, engine_type, car_type, raiting, status, available, CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT)
  values ('123456789ABCDEFGH', 'J13AJC', 1, 2016, 'Black', 5, 'GAS', 'SEDAN', NULL, 'ACTIVE', 1, now(), now(), now());

insert into car (serial, license_plate, model_id, year, color, seat_count, engine_type, car_type, raiting, status, available, CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT)
  values ('123456789ABCDEFGI', 'J13AJD', 2, 2017, 'White', 5, 'HYBRID', 'SEDAN', 4.5, 'ACTIVE', 1, now(), now(), now());

  insert into car (serial, license_plate, model_id, year, color, seat_count, engine_type, car_type, raiting, status, available, CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT)
  values ('123456789ABCDEFGJ', 'J13AJE', 3, 2018, 'White', 5, 'ELECTRIC', 'HATCHBACK', 4.9, 'ACTIVE', 1, now(), now(), now());

insert into car (serial, license_plate, model_id, year, color, seat_count, engine_type, car_type, raiting, status, available, CREATED_AT , UPDATED_AT  , STATUS_UPDATED_AT)
  values ('123456789ABCDEFGG', 'J13AJF', 3, 2018, 'White', 5, 'ELECTRIC', 'HATCHBACK', 4.9, 'INACTIVE', 1, now(), now(), now());



insert into driver_car_map (id, created_at, status, status_updated_at, updated_at, car_id, driver_id) values (null, now(), 'ACTIVE', now(), now(), 1, 1);
insert into driver_car_map (id, created_at, status, status_updated_at, updated_at, car_id, driver_id) values (null, now(), 'ACTIVE', now(), now(), 2, 2);
insert into driver_car_map (id, created_at, status, status_updated_at, updated_at, car_id, driver_id) values (null, now(), 'ACTIVE', now(), now(), 3, 3);
