package com.mytaxi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytaxi.controller.mapper.ServiceModelMapper;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.CarManufacturerDO;
import com.mytaxi.domainobject.CarModelDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.CarType;
import com.mytaxi.domainvalue.EngineType;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

import static org.springframework.test.util.AssertionErrors.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CarControllerTest extends ControllerBaseSetup {

    @Test
    public void getCarById_should_return_200() throws Exception {

        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);
        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        this.mvc.perform(get("/v1/cars/" + carDO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("licensePlate").value(carDO.getLicensePlate()))
                .andExpect(jsonPath("serial").value(carDO.getSerial()));
    }

    @Test
    public void getCarById_should_return_Not_Found_404() throws Exception {

        this.mvc.perform(get("/v1/cars/" + 99999)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createCar_should_return_Created_201() {


        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);

        CarDO carDO = new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true);


        HttpEntity<CarDTO> entity = new HttpEntity<>(ServiceModelMapper.toDto(carDO), headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/cars/"),
                HttpMethod.POST, entity, String.class);

        assertTrue("Should be 201", response.getStatusCode().equals(HttpStatus.CREATED));
    }

    @Test
    public void createCar_should_return_ConstraintViolation_Bad_Request_400() {

        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);

        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));


        HttpEntity<CarDTO> entity = new HttpEntity<>(ServiceModelMapper.toDto(carDO), headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/cars/"),
                HttpMethod.POST, entity, String.class);

        assertTrue("Create Car Should return 400 ConstraintViolation",
                response.getStatusCode().equals(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void updateCar_should_return_Success_200() throws IOException {

        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);

        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        String colorTest = "TestColor";
        CarDTO carDTO = ServiceModelMapper.toDto(carDO);
        carDTO.setColor(colorTest);


        HttpEntity<CarDTO> entity = new HttpEntity<>(carDTO, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/cars/" + carDO.getId()),
                HttpMethod.PUT, entity, String.class);

        ObjectMapper objectMapper = new ObjectMapper();
        CarDTO responseCarDTO = objectMapper.readValue(response.getBody(), CarDTO.class);

        assertTrue("Update Car info should be successful",
                response.getStatusCode().equals(HttpStatus.OK));
        assertTrue("Update Car info should be successful and values should be as expected",
                responseCarDTO.getColor().equals(colorTest));
    }
}
