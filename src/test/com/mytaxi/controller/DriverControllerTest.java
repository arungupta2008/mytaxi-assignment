package com.mytaxi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytaxi.datatransferobject.CarDTO;
import com.mytaxi.datatransferobject.DriverDTO;
import com.mytaxi.domainobject.CarDO;
import com.mytaxi.domainobject.CarManufacturerDO;
import com.mytaxi.domainobject.CarModelDO;
import com.mytaxi.domainobject.DriverCarMappingDO;
import com.mytaxi.domainobject.DriverDO;
import com.mytaxi.domainvalue.CarType;
import com.mytaxi.domainvalue.EngineType;
import com.mytaxi.domainvalue.OnlineStatus;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Objects;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.util.AssertionErrors.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;


/**
 * Created by merom on 18/09/18.
 */

public class DriverControllerTest extends ControllerBaseSetup {


    @Test
    public void givenDriverId_whenGetDriverIfo_thenStatus200()
            throws Exception {

        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        this.mvc.perform(get("/v1/drivers/" + driverDO.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("username").value(driverDO.getUsername()));
    }


    @Test
    public void givenDriverId_whenGetDriverIfo_thenStatus404()
            throws Exception {

        this.mvc.perform(get("/v1/drivers/99999")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }


    @Test
    public void createDriver_should_return_Created_201() {

        DriverDTO.DriverDTOBuilder driverDTOBuilder = DriverDTO.newBuilder()
                .setPassword(getRandomString())
                .setUsername(getRandomString());

        HttpEntity<DriverDTO> entity = new HttpEntity<>(driverDTOBuilder.createDriverDTO(), headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/"),
                HttpMethod.POST, entity, String.class);

        assertTrue("Should be 201", response.getStatusCode().equals(HttpStatus.CREATED));
    }

    @Test
    public void createDriver_should_return_BadReQuest_400() {

        String driverName = getRandomString();
        String driverPassword = getRandomString();
        DriverDTO.DriverDTOBuilder driverDTOBuilder = DriverDTO.newBuilder()
                .setPassword(driverName)
                .setUsername(driverPassword);

        DriverDTO driverDTO = driverDTOBuilder.createDriverDTO();

        HttpEntity<DriverDTO> entity = new HttpEntity<>(driverDTO, headers);

        restTemplate.exchange(
                createURLWithPort("/v1/drivers/"),
                HttpMethod.POST, entity, String.class);

        HttpEntity<DriverDTO> entity1 = new HttpEntity<>(driverDTO, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/"),
                HttpMethod.POST, entity1, String.class);

        assertTrue("Should be 400, because Driver with same username Already existed in System",
                response.getStatusCode()
                        .equals(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void updateLocation_should_return_Success_200() {
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));
        HttpEntity<DriverDTO> entity = new HttpEntity<>(null, headers);
        Double latitude = 1.99;
        Double longitude = 1.22333;

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/" + driverDO.getId() + "?longitude=" + longitude +
                        "&latitude=" + latitude),
                HttpMethod.PUT, entity, String.class);

        assertTrue("Should respond with OK", response.getStatusCode()
                .equals(HttpStatus.OK));
    }

    @Test
    public void updateLocation_should_return_NotFound_404() {

        HttpEntity<DriverDTO> entity = new HttpEntity<>(null, headers);
        Double latitude = 1.99;
        Double longitude = 1.22333;

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/9999" + "?longitude=" + longitude + "&latitude=" + latitude),
                HttpMethod.PUT, entity, String.class);

        assertTrue("Should respond with Not Found", response.getStatusCode()
                .equals(HttpStatus.NOT_FOUND));
    }

    @Test
    public void selectCar_should_return_OK_200() {
        // Car Setup
        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);
        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        // Driver Setup
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString(), OnlineStatus.ONLINE));


        HttpEntity<DriverDTO> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/" + driverDO.getId() + "/car/select/" + carDO.getId()),
                HttpMethod.PUT, entity, String.class);

        assertTrue("Should be able to update the Car", response.getStatusCode()
                .equals(HttpStatus.OK));
    }

    @Test
    public void selectCar_should_return_Not_Allowed_409_when_driver_is_offline() {
        // Car Setup
        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);
        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        // Driver Setup
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));


        HttpEntity<DriverDTO> entity = new HttpEntity<>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/" + driverDO.getId() + "/car/select/" + carDO.getId()),
                HttpMethod.PUT, entity, String.class);

        assertTrue("Should be able to update the Car", response.getStatusCode()
                .equals(HttpStatus.CONFLICT));
    }

    @Test
    public void selectCar_should_return_NotAllowed_Conflict_409() {

        // Car Setup
        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);
        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        CarManufacturerDO carManufacturerDO1 = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO1 = new CarModelDO(getRandomString(), carManufacturerDO1);
        CarDO carDO1 = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO1, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        // Driver Setup carDO
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString(), OnlineStatus.ONLINE));

        // Map Driver with
        DriverCarMappingDO driverCarMappingDO =
                driverCarMappingRepository.save(new DriverCarMappingDO(driverDO, carDO));


        HttpEntity<DriverDTO> entity = new HttpEntity<DriverDTO>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/" + driverDO.getId() + "/car/select/" + carDO1.getId()),
                HttpMethod.PUT, entity, String.class);

        assertTrue("Should raise Conflict if Already assigned to Some other car", response.getStatusCode()
                .equals(HttpStatus.CONFLICT));
    }

    @Test
    public void deSelectCar_should_return_OK_200() {
        // Car Setup
        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);
        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        // Driver Setup
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        // Map Driver with
        DriverCarMappingDO driverCarMappingDO =
                driverCarMappingRepository.save(new DriverCarMappingDO(driverDO, carDO));


        HttpEntity<DriverDTO> entity = new HttpEntity<DriverDTO>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/" + driverDO.getId() + "/car/deselect/" + carDO.getId()),
                HttpMethod.PUT, entity, String.class);

        assertTrue("Driver Should be able to DeSelect the Car", response.getStatusCode()
                .equals(HttpStatus.OK));
    }

    @Test
    public void deSelectCar_should_return_NOT_FOUND_404() {

        // Car Setup
        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);
        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        CarManufacturerDO carManufacturerDO1 = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO1 = new CarModelDO(getRandomString(), carManufacturerDO1);
        CarDO carDO1 = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO1, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        // Driver Setup carDO
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        // Map Driver with
        DriverCarMappingDO driverCarMappingDO =
                driverCarMappingRepository.save(new DriverCarMappingDO(driverDO, carDO));


        HttpEntity<DriverDTO> entity = new HttpEntity<DriverDTO>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/" + driverDO.getId() + "/car/deselect/" + 999999),
                HttpMethod.PUT, entity, String.class);

        assertTrue("Driver Should fail to Deselect the car if Car not connected to Driver Currently",
                response.getStatusCode()
                        .equals(HttpStatus.NOT_FOUND));
    }

    @Test
    public void updateDriverStatus_should_return_OK_200() throws IOException {
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        HttpEntity<DriverDTO> entity = new HttpEntity<DriverDTO>(null, headers);
        OnlineStatus status = OnlineStatus.ONLINE;
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/" + driverDO.getId() + "/status/" + status),
                HttpMethod.PUT, entity, String.class);

        assertTrue("Driver should be able to update the Online status",
                response.getStatusCode()
                        .equals(HttpStatus.OK));

        assertTrue("Driver response should have status as online",
                Objects.requireNonNull(response.getBody()).contains(status.name()));
    }

    @Test
    public void updateDriverStatus_should_return_Not_Found_404() throws IOException {

        HttpEntity<DriverDTO> entity = new HttpEntity<DriverDTO>(null, headers);
        OnlineStatus status = OnlineStatus.ONLINE;
        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/v1/drivers/" + 999999 + "/status/" + status),
                HttpMethod.PUT, entity, String.class);

        assertTrue("Driver should be able to update the Online status",
                response.getStatusCode()
                        .equals(HttpStatus.NOT_FOUND));
    }

    @Test
    public void search_drivers_by_userName_should_return_driver_for_query_equal() throws Exception {
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));


        this.mvc.perform(get("/v1/drivers/search?searchKeys=driver.username="+driverDO.getUsername())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].username").value(driverDO.getUsername()));
                //.andExpect(jsonPath("username").value(driverDO.getUsername()));

    }

    @Test
    public void search_drivers_by_userName_should_return_driver_for_query_like() throws Exception {
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));


        this.mvc.perform(get("/v1/drivers/search?searchKeys=driver.username:"+driverDO.getUsername())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].username").value(driverDO.getUsername()));
        //.andExpect(jsonPath("username").value(driverDO.getUsername()));

    }

    @Test
    public void search_drivers_by_car_serial_should_return_driver_for_query_wildcard() throws Exception {

        // Car Setup
        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);
        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        // Driver Setup
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        // Map Driver with
        DriverCarMappingDO driverCarMappingDO =
                driverCarMappingRepository.save(new DriverCarMappingDO(driverDO, carDO));


        this.mvc.perform(get("/v1/drivers/search?searchKeys=car.serial:" + carDO.getSerial())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].username").value(driverDO.getUsername()));

    }

    @Test
    public void search_drivers_by_car_seat_count_should_return_drivers_for_query_less_than() throws Exception {

        // Car Setup
        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);
        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        // Driver Setup
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        // Map Driver with
        DriverCarMappingDO driverCarMappingDO =
                driverCarMappingRepository.save(new DriverCarMappingDO(driverDO, carDO));

        // Other Car
        // Car Setup
        CarManufacturerDO carManufacturerDO1 = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO1 = new CarModelDO(getRandomString(), carManufacturerDO1);
        CarDO carDO1 = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO1, 2000,
                getRandomString(), 2, EngineType.ELECTRIC, CarType.COMPACT, 4.5, true));

        // Driver Setup
        DriverDO driverDO1 = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        // Map Driver with
        DriverCarMappingDO driverCarMappingDO1 =
                driverCarMappingRepository.save(new DriverCarMappingDO(driverDO1, carDO1));


        this.mvc.perform(get("/v1/drivers/search?searchKeys=car.seat_count<3")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].username").value(driverDO.getUsername()))
                .andExpect(jsonPath("$[1].username").value(driverDO1.getUsername()));

    }


    @Test
    public void search_drivers_by_car_seat_count_should_return_drivers_for_query_greater_than() throws Exception {

        // Car Setup
        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);
        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 30, EngineType.ELECTRIC, CarType.VAN, 4.5, true));

        // Driver Setup
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        // Map Driver with
        DriverCarMappingDO driverCarMappingDO =
                driverCarMappingRepository.save(new DriverCarMappingDO(driverDO, carDO));

        // Other Car
        // Car Setup
        CarManufacturerDO carManufacturerDO1 = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO1 = new CarModelDO(getRandomString(), carManufacturerDO1);
        CarDO carDO1 = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO1, 2000,
                getRandomString(), 30, EngineType.ELECTRIC, CarType.VAN, 4.5, true));

        // Driver Setup
        DriverDO driverDO1 = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        // Map Driver with
        DriverCarMappingDO driverCarMappingDO1 =
                driverCarMappingRepository.save(new DriverCarMappingDO(driverDO1, carDO1));


        this.mvc.perform(get("/v1/drivers/search?searchKeys=car.seat_count>29")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].username").value(driverDO.getUsername()))
                .andExpect(jsonPath("$[1].username").value(driverDO1.getUsername()));

    }

    @Test
    public void search_drivers_by_car_type_should_return_drivers_for_query_equal() throws Exception {

        // Car Setup
        CarManufacturerDO carManufacturerDO = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO = new CarModelDO(getRandomString(), carManufacturerDO);
        CarDO carDO = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO, 2000,
                getRandomString(), 10, EngineType.ELECTRIC, CarType.CROSSOVER, 4.5, true));

        // Driver Setup
        DriverDO driverDO = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        // Map Driver with
        DriverCarMappingDO driverCarMappingDO =
                driverCarMappingRepository.save(new DriverCarMappingDO(driverDO, carDO));

        // Other Car
        // Car Setup
        CarManufacturerDO carManufacturerDO1 = new CarManufacturerDO(getRandomString());
        CarModelDO carModelDO1 = new CarModelDO(getRandomString(), carManufacturerDO1);
        CarDO carDO1 = carRepository.save(new CarDO(getRandomString(), getRandomString(), carModelDO1, 2000,
                getRandomString(), 13, EngineType.ELECTRIC, CarType.CROSSOVER, 4.5, true));

        // Driver Setup
        DriverDO driverDO1 = driverRepository.save(new DriverDO(getRandomString(), getRandomString()));

        // Map Driver with
        DriverCarMappingDO driverCarMappingDO1 =
                driverCarMappingRepository.save(new DriverCarMappingDO(driverDO1, carDO1));


        this.mvc.perform(get("/v1/drivers/search?searchKeys=car.car_type=CROSSOVER")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].username").value(driverDO.getUsername()))
                .andExpect(jsonPath("$[1].username").value(driverDO1.getUsername()));

    }
}