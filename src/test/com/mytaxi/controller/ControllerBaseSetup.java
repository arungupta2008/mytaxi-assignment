package com.mytaxi.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytaxi.MytaxiServerApplicantTestApplication;
import com.mytaxi.dataaccessobject.CarRepository;
import com.mytaxi.dataaccessobject.DriverCarMappingRepository;
import com.mytaxi.dataaccessobject.DriverRepository;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = MytaxiServerApplicantTestApplication.class)
@AutoConfigureMockMvc
public class ControllerBaseSetup {

    @Autowired
    MockMvc mvc;

    @LocalServerPort
    int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    @Autowired
    CarRepository carRepository;

    @Autowired
    DriverCarMappingRepository driverCarMappingRepository;

    @Autowired
    DriverRepository driverRepository;

    public String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getRandomString() {
        return UUID.randomUUID().toString();
    }

    public String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}
